resource "aws_iam_role" "cluster" {
  count = var.manage_cluster_iam_resources && var.create_eks ? 1 : 0

  name_prefix           = var.cluster_iam_role_name != "" ? null : var.cluster_name
  name                  = var.cluster_iam_role_name != "" ? var.cluster_iam_role_name : null
  assume_role_policy    = data.aws_iam_policy_document.cluster_assume_role_policy.json
  permissions_boundary  = var.permissions_boundary
  path                  = var.iam_path
  force_detach_policies = true

  tags = var.tags
}

#cluster depend on
resource "aws_iam_role_policy_attachment" "cluster_AmazonEKSClusterPolicy" {
  count = var.manage_cluster_iam_resources && var.create_eks ? 1 : 0

  policy_arn = "${local.policy_arn_prefix}/AmazonEKSClusterPolicy"
  role       = local.cluster_iam_role_name
}

#cluster depend on
resource "aws_iam_role_policy_attachment" "cluster_AmazonEKSServicePolicy" {
  count = var.manage_cluster_iam_resources && var.create_eks ? 1 : 0

  policy_arn = "${local.policy_arn_prefix}/AmazonEKSServicePolicy"
  role       = local.cluster_iam_role_name
}

#cluster depend on
resource "aws_iam_role_policy_attachment" "cluster_AmazonEKSVPCResourceControllerPolicy" {
  count = var.manage_cluster_iam_resources && var.create_eks ? 1 : 0

  policy_arn = "${local.policy_arn_prefix}/AmazonEKSVPCResourceController"
  role       = local.cluster_iam_role_name
}

data "aws_iam_policy_document" "cluster_elb_sl_role_creation" {
  count = var.manage_cluster_iam_resources && var.create_eks ? 1 : 0

  statement {
    effect = "Allow"
    actions = [
      "ec2:DescribeAccountAttributes",
      "ec2:DescribeInternetGateways",
      "ec2:DescribeAddresses"
    ]
    resources = ["*"]
  }
}

resource "aws_iam_policy" "cluster_elb_sl_role_creation" {
  count = var.manage_cluster_iam_resources && var.create_eks ? 1 : 0

  name_prefix = "${var.cluster_name}-elb-sl-role-creation"
  description = "Permissions for EKS to create AWSServiceRoleForElasticLoadBalancing service-linked role"
  policy      = data.aws_iam_policy_document.cluster_elb_sl_role_creation[0].json
  path        = var.iam_path

  tags = var.tags
}

resource "aws_iam_role_policy_attachment" "cluster_elb_sl_role_creation" {
  count = var.manage_cluster_iam_resources && var.create_eks ? 1 : 0

  policy_arn = aws_iam_policy.cluster_elb_sl_role_creation[0].arn
  role       = local.cluster_iam_role_name
}

##########################################################################################################
# Worker
##########################################################################################################

resource "aws_iam_role" "workers" {
  count = var.manage_worker_iam_resources && var.create_eks ? 1 : 0

  name_prefix           = var.workers_role_name != "" ? null : local.cluster_name
  name                  = var.workers_role_name != "" ? var.workers_role_name : null
  assume_role_policy    = data.aws_iam_policy_document.workers_assume_role_policy.json
  permissions_boundary  = var.permissions_boundary
  path                  = var.iam_path
  force_detach_policies = true

  tags = var.tags
}

# resource "aws_iam_instance_profile" "workers" {
#   count = var.manage_worker_iam_resources && var.create_eks ? local.worker_group_launch_configuration_count : 0

#   name_prefix = local.cluster_name
#   role = lookup(
#     var.worker_groups[count.index],
#     "iam_role_id",
#     local.default_iam_role_id,
#   )
#   path = var.iam_path

#   tags = var.tags

#   lifecycle {
#     create_before_destroy = true
#   }
# }

resource "aws_iam_role_policy_attachment" "workers_AmazonEKSWorkerNodePolicy" {
  count = var.manage_worker_iam_resources && var.create_eks ? 1 : 0

  policy_arn = "${local.policy_arn_prefix}/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.workers[0].name
}

resource "aws_iam_role_policy_attachment" "workers_AmazonEKS_CNI_Policy" {
  count = var.manage_worker_iam_resources && var.attach_worker_cni_policy && var.create_eks ? 1 : 0

  policy_arn = "${local.policy_arn_prefix}/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.workers[0].name
}

resource "aws_iam_role_policy_attachment" "workers_AmazonEC2ContainerRegistryReadOnly" {
  count = var.manage_worker_iam_resources && var.create_eks ? 1 : 0

  policy_arn = "${local.policy_arn_prefix}/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.workers[0].name
}

############################################################################################################
#### 추가 권한 설정 예) s3 접근 권한 등...
resource "aws_iam_role_policy_attachment" "workers_additional_policies" {
  count = var.manage_worker_iam_resources && var.create_eks ? length(var.workers_additional_policies) : 0

  role       = aws_iam_role.workers[0].name
  policy_arn = var.workers_additional_policies[count.index]
}