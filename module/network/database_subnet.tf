# subnet private

resource "aws_subnet" "database" {
  count = length(var.database_subnets)

  vpc_id = local.vpc_id

  availability_zone = var.database_subnets[count.index].zone
  cidr_block        = var.database_subnets[count.index].cidr

  tags = merge(
    {
      Name = format(
        "%s-%s",
        var.name,
        var.database_subnets[count.index].name
      )
    },
    var.database_subnets[count.index].tags,
    var.tags,
  )
}

resource "aws_route_table_association" "database" {
  count = local.nat_gateway_count > 0 ? length(var.database_subnets) : 0

  subnet_id = aws_subnet.database[count.index].id

  route_table_id = element(
    aws_route_table.private.*.id,
    var.single_nat_gateway ? 0 : local.zone_index[element(split("", var.private_subnets[count.index].zone), length(var.private_subnets[count.index].zone) - 1)]
  )
}

resource "aws_db_subnet_group" "database" {
  name        = "usertrade_db_subnet"
  description = "Database subnet group for usertrade"
  subnet_ids  = aws_subnet.database.*.id
}