#output

output "vpc_id" {
  value = module.vpc.vpc_id
}

output "public_subnet_ids" {
  value = module.vpc.public_subnet_ids
}

output "public_subnet_azs" {
  value = module.vpc.public_subnet_azs
}

output "public_subnet_cidr" {
  value = module.vpc.public_subnet_cidr
}

output "public_route_table_ids" {
  value = module.vpc.public_route_table_ids
}

output "private_subnet_ids" {
  value = module.vpc.private_subnet_ids
}

output "private_subnet_azs" {
  value = module.vpc.private_subnet_azs
}

output "private_subnet_cidr" {
  value = module.vpc.private_subnet_cidr
}

output "private_route_table_ids" {
  value = module.vpc.private_route_table_ids
}

output "nat_gateway_ids" {
  value = module.vpc.nat_gateway_ids
}

output "nat_gateway_ips" {
  value = module.vpc.nat_gateway_ips
}

output "db_cluster_endpoint" {
  value = module.aurora.rds_cluster_endpoint
}

output "s3_id" {
  value = aws_s3_bucket.front.id
}

output "s3_domain_name" {
  value = aws_s3_bucket.front.bucket_regional_domain_name
}

output "s3_endpoint" {
  value = aws_s3_bucket.front.website_endpoint
}

output "ecr_url" {
  value = aws_ecr_repository.ghj-app.repository_url
}

output "cluster_security_group_id" {
  value = module.eks.cluster_security_group_id
}

output "cluster_primary_security_group_id" {
  value = module.eks.cluster_primary_security_group_id
}

output "worker_security_group_id" {
  value = module.eks.worker_security_group_id
}