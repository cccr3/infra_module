##########  Network ##########
# variable

variable "region" {
  default = "ap-northeast-2"
}

variable "name" {
  default = "cccr3-usertrade"
}

variable "vpc_id" {
  default = ""
}

variable "vpc_cidr" {
  default = "10.99.0.0/16"
}

variable "single_route_table" {
  default = true
}

variable "enable_nat_gateway" {
  default = true
}

variable "single_nat_gateway" {
  default = true
}

variable "public_subnets" {
  # type = list(object({
  #   zone = string
  #   cidr = string
  #   tags = map
  # }))
  default = [
    {
      name = "public-a"
      zone = "ap-northeast-2a"
      cidr = "10.99.1.0/24"
      tags = {}
    },
    {
      name = "public-b"
      zone = "ap-northeast-2b"
      cidr = "10.99.2.0/24"
      tags = {}
    },
    {
      name = "public-c"
      zone = "ap-northeast-2c"
      cidr = "10.99.3.0/24"
      tags = {}
    },
  ]
}

variable "private_subnets" {
  # type = list(object({
  #   zone = string
  #   cidr = string
  #   tags = map
  # }))
  default = [
    {
      name = "private-a"
      zone = "ap-northeast-2a"
      cidr = "10.99.4.0/24"
      tags = {}
    },
    {
      name = "private-b"
      zone = "ap-northeast-2b"
      cidr = "10.99.5.0/24"
      tags = {}
    },
    {
      name = "private-c"
      zone = "ap-northeast-2c"
      cidr = "10.99.6.0/24"
      tags = {}
    },
  ]
}

variable "database_subnets" {
  # type = list(object({
  #   zone = string
  #   cidr = string
  #   tags = map
  # }))
  default = [
    {
      name = "database-a"
      zone = "ap-northeast-2a"
      cidr = "10.99.7.0/24"
      tags = {}
    },
    {
      name = "database-b"
      zone = "ap-northeast-2b"
      cidr = "10.99.8.0/24"
      tags = {}
    },
    {
      name = "database-c"
      zone = "ap-northeast-2c"
      cidr = "10.99.9.0/24"
      tags = {}
    },
  ]
}

variable "tags" {
  description = "A map of tags to add to all resources"
  default = {
    "kubernetes.io/cluster/eks-public" = "shared"
  }
}
//########### EKS ##########
//variable "cluster_name_prefix" {
//  type = string
//}
//
//variable "cluster_version" {
//  type = string
//}
//
//variable "node_group_name" {
//  type = string
//}
//
//variable "node_instance_type" {
//  type = list(string)
//}
//
//variable "node_desired_capacity" {
//  type = number
//}
//
//variable "node_max_capacity" {
//  type = number
//}
//
//variable "node_min_capacity" {
//  type = number
//}
//
//variable "node_group_tags" {
//  type = map(string)
//}
//
//##########  DB  ##########
//variable "engine_mode" {
//  type = string
//}
//
//variable "db_instance_type" {
//  type = string
//}
//
//variable "db_allowed_cidr_blocks" {
//  type = list(string)
//}
//
//variable "db_replica_count" {
//  type = number
//}
//
//variable "db_pg_name" {
//  type = string
//}
//
//variable "pg_family" {
//  type = string
//}

