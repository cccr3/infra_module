terraform {
    backend "s3" {
      region         = "ap-northeast-2"
      bucket         = "cccr3-prj-test-tfstate"
      key            = "cccr3-prj.tfstate"
      dynamodb_table = "cccr3-prj-test-lock"
      encrypt        = true
    }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.60.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.5.0"
    }
  }
}

provider "aws" {
  region  = var.region
  profile = "default"
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.eks.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.eks.token
}

locals {
  db_name = "mysql"
  tags = {
    own = "cccr3"
    env = "test"
  }
}

########################################################################################################################
# EKS_data_source
########################################################################################################################
data "aws_eks_cluster" "eks" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "eks" {
  name = module.eks.cluster_id
}

########################################################################################################################
# DB_Password
########################################################################################################################
resource "random_password" "master" {
  length = 10
}

resource "aws_secretsmanager_secret" "master" {
  name = "cccr3_db_master"
}

resource "aws_secretsmanager_secret_version" "master" {
  secret_id     = aws_secretsmanager_secret.master.id
  secret_string = random_password.master.result
}

########################################################################################################################
# DB Parameter
########################################################################################################################
resource "aws_db_parameter_group" "db-pg" {
  name        = "${local.db_name}-aurora-db-56-parameter-group"
  family      = "aurora5.6"
  description = "${local.db_name}-aurora-db-56-parameter-group"
  tags        = local.tags
}

resource "aws_rds_cluster_parameter_group" "rds-pg" {
  name        = "${local.db_name}-aurora-56-cluster-parameter-group"
  family      = "aurora5.6"
  description = "${local.db_name}-aurora-56-cluster-parameter-group"
  tags        = local.tags

  parameter {
    name  = "time_zone"
    value = "Asia/Seoul"
  }

  parameter {
    name  = "character_set_server"
    value = "utf8mb4"
  }

  parameter {
    name  = "character_set_client"
    value = "utf8mb4"
  }

  parameter {
    name  = "character_set_connection"
    value = "utf8mb4"
  }

  parameter {
    name  = "character_set_database"
    value = "utf8mb4"
  }

  parameter {
    name  = "character_set_filesystem"
    value = "utf8mb4"
  }

  parameter {
    name  = "character_set_results"
    value = "utf8mb4"
  }

  parameter {
    name  = "collation_connection"
    value = "utf8mb4_general_ci"
  }

  parameter {
    name  = "collation_server"
    value = "utf8mb4_general_ci"
  }
}

########################################################################################################################
# Network
########################################################################################################################

module "vpc" {
  source = "../module/network"

  region = var.region
  name   = var.name

  vpc_id   = var.vpc_id
  vpc_cidr = var.vpc_cidr

  single_route_table = var.single_route_table

  enable_nat_gateway = var.enable_nat_gateway
  single_nat_gateway = var.single_nat_gateway

  public_subnets  = var.public_subnets
  private_subnets = var.private_subnets
  database_subnets = var.database_subnets

  tags = local.tags
}

########################################################################################################################
# Bastion Host
########################################################################################################################
resource "aws_instance" "Bastion" {
  subnet_id = module.vpc.public_subnet_ids[0]
  instance_type = "t3.small"
  availability_zone = "ap-northeast-2a"
  ami = "ami-0e4a9ad2eb120e054"

  associate_public_ip_address = true

  tags = merge(local.tags, {"Name" = "Bastion"})

  security_groups = [aws_security_group.Bastion.id]
}

resource "aws_security_group" "Bastion" {
  name = "bastion-sg"
  vpc_id = module.vpc.vpc_id
}

resource "aws_security_group_rule" "ingress_ssh" {
  from_port         = 22
  protocol          = "tcp"
  security_group_id = aws_security_group.Bastion.id
  to_port           = 22
  cidr_blocks = ["0.0.0.0/0"]
  type              = "ingress"
}

resource "aws_security_group_rule" "ingress_mysql" {
  from_port         = 3306
  protocol          = "tcp"
  security_group_id = aws_security_group.Bastion.id
  to_port           = 3306
  cidr_blocks = module.vpc.db_subnet_cidr
  type              = "ingress"
}

resource "aws_security_group_rule" "allow_all" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.Bastion.id
  to_port           = 0
  cidr_blocks = ["0.0.0.0/0"]
  type              = "egress"
}

########################################################################################################################
#EKS
########################################################################################################################
module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "17.20.0"
  # insert the 7 required variables here

  cluster_name    = "cccr3-cluster"
  cluster_version = "1.20"
  subnets         = module.vpc.private_subnet_ids
  vpc_id          = module.vpc.vpc_id

  write_kubeconfig = false

  # 추가 oidc, 워커의 s3 접근 정책 추가
  enable_irsa = true
  workers_additional_policies = ["arn:aws:iam::aws:policy/AmazonS3FullAccess"]

  map_users = [
    {
      userarn  = "arn:aws:iam::371156277055:user/jghee"
      username = "jghee"
      groups   = ["system:masters"]

    },    

    {
      userarn  = "arn:aws:iam::371156277055:user/jadenpark"
      username = "jadenpark"
      groups   = ["system:masters"]

    },
    {
      userarn  = "arn:aws:iam::371156277055:user/rlatkddyd987"
      username = "rlatkddyd987"
      groups   = ["system:masters"]

    },
    {
      userarn  = "arn:aws:iam::371156277055:user/mzmz01"
      username = "mzmz01"
      groups   = ["system:masters"]

    }
  ]

  node_groups = {
    example = {
      name_prefix      = "cccr3-nodegroup"
      desired_capacity = 3
      max_capacity     = 10
      min_capacity     = 1

      instance_types = ["m5.xlarge"]

      additional_tags = local.tags
    }
  }

}
########################################################################################################################
# K8S namespace
########################################################################################################################
resource "kubernetes_namespace" "backend" {
  metadata {
    annotations = {
      name = "backend"
    }

    labels = {
      env = "test"
    }

    name = "back"
  }
}

resource "kubernetes_namespace" "argo" {
  metadata {
    annotations = {
      name = "Continuous Delivery"
    }

    labels = {
      env = "test"
    }

    name = "argocd"
  }
}

resource "kubernetes_namespace" "monitoring" {
  metadata {
    annotations = {
      name = "Prometheus and Grafana"
    }

    labels = {
      env = "test"
    }

    name = "monitor"
  }
}

resource "kubernetes_namespace" "logging" {
  metadata {
    annotations = {
      name = "Logging"
    }

    labels = {
      env = "test"
    }

    name = "logging"
  }
}

########################################################################################################################
#MultiMaster_Aurora
########################################################################################################################
module "aurora" {
  source = "terraform-aws-modules/rds-aurora/aws"
  version = "5.3.0"

  name          = local.db_name
  engine_mode   = "multimaster"
  instance_type = "db.r4.2xlarge"

  vpc_id                = module.vpc.vpc_id
  db_subnet_group_name  = module.vpc.db_subnet_group_name
  create_security_group = true
#  allowed_cidr_blocks   = concat(module.vpc.private_subnet_cidr, module.vpc.public_subnet_cidr)
  allowed_cidr_blocks   = concat(module.vpc.private_subnet_cidr, [module.vpc.public_subnet_cidr[0]])

  database_name = "USERTRADE"

  replica_count                       = 2
  iam_database_authentication_enabled = false
  password                            = random_password.master.result
  create_random_password              = false

  apply_immediately   = true
  skip_final_snapshot = true

  db_parameter_group_name         = aws_db_parameter_group.db-pg.id
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.rds-pg.id
  #enabled_cloudwatch_logs_exports = ["audit", "error", "general", "slowquery"]

  tags = local.tags
}

########################################################################################################################
# S3
########################################################################################################################
resource "aws_s3_bucket" "front" {
  bucket = "cccr3-front-app"
  acl    = "public-read"
  force_destroy = true
  website {
    index_document = "index.html"
  }
  tags = local.tags
}

resource "aws_s3_bucket" "image_server" {
  bucket = "cccr3-images"
  acl    = "public-read"
  force_destroy = true
  tags = local.tags
}

########################################################################################################################
# ECR
########################################################################################################################
resource "aws_ecr_repository" "ghj-app" {
  name                 = "cccr3_usertrade"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}
###########################################################################################################################
# Cloudfront
###########################################################################################################################
resource "aws_cloudfront_distribution" "CDN" {
  comment = "cccr3-front CDN"

  origin {
    domain_name = aws_s3_bucket.front.bucket_domain_name
    origin_id = aws_s3_bucket.front.id
  }
  enabled = true

  default_cache_behavior {
    allowed_methods = ["GET", "HEAD"]
    cached_methods = ["GET", "HEAD"]
    target_origin_id = aws_s3_bucket.front.id

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
    viewer_protocol_policy = "allow-all"
    min_ttl = 0
    default_ttl = 3600
    max_ttl = 86400
  }

  price_class = "PriceClass_200"


  default_root_object = "index.html"

  viewer_certificate {
    cloudfront_default_certificate = true
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
}